<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hero;

class HeroesController extends Controller
{
    public function index()
    {
        $heroes = Hero::all();
        return Response::json($heroes);
    }

    public function topFour()
    {
        return Hero::take(4)->get();
    }

    public function show($id)
    {
        return Hero::findOrFail($id);
    }

    public function edit(Request $request, $id)
    {
        $hero = Hero::findOrFail($id);
        $hero->name = $request['name'];
        $hero->save();
    }

    public function remove($id)
    {
        $hero = Hero::findOrFail($id);
        $hero->delete();
    }
}
