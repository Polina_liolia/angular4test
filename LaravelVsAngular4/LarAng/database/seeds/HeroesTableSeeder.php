<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HeroesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('heroes')->insert([
            'name' => 'Batman'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Superman'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Spiderman'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Megamind'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Mask'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Thor'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Hellboy'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Iron man'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Hulk'
        ]);
        DB::table('heroes')->insert([
            'name' => 'Wolverine'
        ]);


    }
}
